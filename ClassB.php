<?php


class ClassB
{

    /**
     * @var string $b ;
     */
    protected $b;

    public function doB()
    {
        $this->b .= 'doing B ';
    }

    /**
     * @param string $b
     */
    public function setB($b)
    {
        $this->b = $b;
    }

    /**
     * @return string
     */
    public function getB()
    {
        return $this->b;
    }


} 