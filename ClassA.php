<?php


class ClassA
{

    /**
     * @var string $a ;
     */
    protected $a;

    function __construct($a)
    {
        $this->a = $a;
    }

    public function doA()
    {
        $this->a .= ' doing A ';
    }

    /**
     * @return string
     */
    public function getA()
    {
        return $this->a;
    }


} 