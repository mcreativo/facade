<?php


class Facade
{


    /**
     * @param $string
     * @return string
     */
    public function getNeededObject($string)
    {
        $a = new ClassA($string);
        $b = new ClassB();
        $c = new ClassC();

        $a->doA();
        $b->setB($a->getA());
        $b->doB();
        $c->doC($b);
        return $c->getC();
    }
} 