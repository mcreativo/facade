<?php


class ClassC
{
    /**
     * @var string
     */
    private $c;

    public function doC(ClassB $b)
    {
        $this->c = $b->getB() . 'doing C with object B ';
    }

    /**
     * @return string
     */
    public function getC()
    {
        return $this->c;
    }


} 